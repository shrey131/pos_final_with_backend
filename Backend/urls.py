from django.urls import path

from . import views

urlpatterns = [
  path('products/', views.ProductList.as_view()),
  path('products/<int:pk>/', views.ProductDetail.as_view()),
  path('bill/<int:pk>/', views.BillDetail.as_view()),
  path('bill/', views.BillList.as_view()),
  path('bill/<bill_id>', views.BillDetail.as_view()),
]


