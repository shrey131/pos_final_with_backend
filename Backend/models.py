from django.db import models

class Product(models.Model):
  label = models.CharField(max_length=150, null=False)
  value = models.IntegerField(null=False)
  quantity_available = models.IntegerField(null=False)
  date_added = models.DateTimeField(auto_now_add=True)
  date_updated = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.label


class Bill(models.Model):
  bill_id = models.IntegerField( unique=False)
  label = models.CharField(max_length=150, null=False)
  value = models.IntegerField(null=False)
  quantity = models.IntegerField(null=False)
  total = models.IntegerField(null=False)
  date_added = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.label



  