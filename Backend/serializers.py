from rest_framework import serializers

from . import models

class ProductSerializer(serializers.ModelSerializer):
  class Meta:
    model = models.Product
    fields = '__all__'

class BillSerializer(serializers.ModelSerializer):
  class Meta:
    model = models.Bill
    fields = '__all__'