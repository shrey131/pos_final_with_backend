# Generated by Django 2.0.4 on 2018-05-16 09:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Backend', '0002_auto_20180516_0805'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Products',
            new_name='Product',
        ),
    ]
