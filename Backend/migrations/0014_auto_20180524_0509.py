# Generated by Django 2.0.4 on 2018-05-24 05:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Backend', '0013_auto_20180524_0401'),
    ]

    operations = [
        migrations.AddField(
            model_name='bill',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='bill',
            name='bill_id',
            field=models.IntegerField(),
        ),
    ]
