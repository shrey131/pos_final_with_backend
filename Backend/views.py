from rest_framework import generics

from . import models
from . import serializers

class ProductList(generics.ListCreateAPIView):
  queryset = models.Product.objects.all()
  serializer_class = serializers.ProductSerializer

class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
  queryset = models.Product.objects.all()
  serializer_class = serializers.ProductSerializer

class BillList(generics.ListCreateAPIView):
  queryset = models.Bill.objects.all()
  serializer_class = serializers.BillSerializer

class BillDetail(generics.ListAPIView):
  serializer_class = serializers.BillSerializer

  def get_queryset(self):
      
    bill_id = self.kwargs['bill_id']
    return models.Bill.objects.filter(bill_id=bill_id)