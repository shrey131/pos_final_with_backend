from django.contrib import admin

from .models import Product
from .models import Bill

admin.site.register(Product)
admin.site.register(Bill)

