This project was created using Django and Django Rest Framework for backend. And Pure ReactJS for frontend. 

This project I have created is for shopping malls/big stores (like big bazaar) billing counter. This creates the bill and displays a QR-Code instead of the printed bill. You can scan the QR-Code and the bill will come on your phone screen. This `e-bill` can be used for reference, returning of the products,etc.

This project was created keeping in mind the need to save paper in order to save the environment, and also because many people just throw away the long paper bill and thus they can't refer to it later.

Next I plan to create a mobile app that will store these e-bills and do some data analysis on them.
