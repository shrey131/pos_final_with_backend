import React, { Component } from 'react';

import { Home } from './containers/Home/Home';
import { Route, Link } from 'react-router-dom';
import { POSScreen } from './containers/POSScreen/POSScreen';
import { Checkout } from './containers/Checkout/Checkout';
import { QR_Code } from './containers/QRCode/QRCode';
import { UpdateStock } from './containers/UpdateStock/UpdateStock';
import PageShell from './components/PageShell/PageShell';

export class App extends Component {
  render() {
    return (
      <div>
        <div className="App">
        <div className="TopBar">
        <Link to="/">Home</Link>
        <Link to="/pos">POS</Link>
       </div>
        <Route path="/pos" exact component={PageShell(POSScreen)} />
        <Route path="/" exact component={Home} />
        <Route path="/update" exact component={PageShell(UpdateStock)} />
        <Route path="/checkout" exact component={PageShell(Checkout)} />
        <Route path="/qrcode" exact component={PageShell(QR_Code)} />
      </div>

      </div>
    );
  }
}
