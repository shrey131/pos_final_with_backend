
import Cancel from 'react-icons/lib/io/android-cancel';
import IoAndroidAddCircle from 'react-icons/lib/io/android-add-circle';
import IoIosMinus from 'react-icons/lib/io/ios-minus';
import {Table} from 'reactstrap';
import TransitionGroup from 'react-transition-group/TransitionGroup';
import CSSTransition from 'react-transition-group/CSSTransition';
import  './ProductList.css';

import React, { Component } from 'react';
const style= {
  color: 'black'
};
export class ProductList extends Component {

    render() {

      let rowComponent = this.props.ItemsList.map((product, index) => (
        <CSSTransition classNames="fade" timeout={1000}>
          <tr>
            <th style={style} scope='row'>{index+1}</th>
            <td style={style} >{product.label}</td>
            <td style={style} >{product.value}</td>
            <td style={style} >{product.quantity_selected}</td>
            <td style={style} >{product.total}</td>
            <td  style={style} onClick={() => this.props.incrementItem(index)}><IoAndroidAddCircle /></td>
            <td  style={style} onClick={() => this.props.decrementItem(index)}><IoIosMinus /></td>
            <td  style={style} onClick={() => this.props.deleteItem(index)}><Cancel /></td>
          </tr>
          </CSSTransition>
        ));


    return (
      <Table>
      <thead>
        <tr>
          <th style={style} >#</th>
          <th style={style} >Product</th>
          <th style={style} >Rate</th>
          <th style={style} >Quantity</th>
          <th style={style} >Total</th>
        </tr>
      </thead>
      <TransitionGroup
        component="tbody">
        {rowComponent}
      </TransitionGroup>
      </Table>
    )
  }
}
