import React from 'react';
import './QRCode.css';
import axios from 'axios';
import QRCode from 'react-qr-code';

export class QR_Code extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      bill_id: 0
    }
  }
  componentDidMount(){
    let data = [];
    axios.get("http://127.0.0.1:8000/api/bill/")
    .then(response => {
      
      data = response.data;
      
      let tempBillID = data[0].bill_id;
    data.map((item,index) => {
      let temp_bill_id = item.bill_id;
      temp_bill_id > tempBillID ?
      (
        tempBillID = temp_bill_id
      ):(
        null
      );
    });
    this.setState({
      bill_id: tempBillID
    });
    
    });
  }

  render() {
    let valueString = "http://127.0.0.1:8000/api/bill/" + this.state.bill_id;
    
    
    return (
      <div className="qrcode">
        <QRCode value={valueString}/>
      </div>
    );
  }
}
