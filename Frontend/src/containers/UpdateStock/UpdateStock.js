import React from 'react';
import {fetchProducts, updateProduct} from '../../api';
import { Table } from 'reactstrap';
import './UpdateStock.css';
import axios from 'axios';
import { AddProduct } from '../AddProduct/AddProduct';
import IoAndroidDelete from 'react-icons/lib/io/android-delete';

export class UpdateStock extends React.Component {
  constructor(props){
    super(props)
    this.state={
      options: [],
      name: "",
      rate: 0,
      quantity: 0,
      dummy: 0
    }
    this.getData = this.getData.bind(this);
    this.handleNameClick= this.handleNameClick.bind(this);
    this.handleRateClick = this.handleRateClick.bind(this);
    this.handleQuantityClick = this.handleQuantityClick.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  componentDidMount() {
    this.getData();
  }

  async getData() {
    let data = await fetchProducts();
    this.setState({options: data});
    console.log(this.state.options);
  }

  async handleNameClick(index, name, id, rate , quantity) {
    let new_name = prompt("Enter new name");
    let data = {
      label: new_name,
      value: rate,
      quantity_available: quantity
    };
    await axios.put("http://127.0.0.1:8000/api/products/"+id+"/", data )
    .then(response => {
      console.log(response);
      let temp_array = this.state.options;
      temp_array[index].label = response.data.label;
      this.setState({
        options: temp_array
      })
    });
  }

  async handleRateClick (index, name, id, rate , quantity) {
    let new_rate = prompt("Enter new rate");
    let data = {
      label: name,
      value: new_rate,
      quantity_available: quantity
    };
    await axios.put("http://127.0.0.1:8000/api/products/"+id+"/", data )
    .then(response => {
      console.log(response);
      let temp_array = this.state.options;
      temp_array[index].value= response.data.value;
      this.setState({
        options: temp_array
      })
    });
  }

  async handleQuantityClick(index, name, id, rate , quantity) {
    let new_quantity = prompt("Enter new quantity");
    let data = {
      label: name,
      value: rate,  
      quantity_available: new_quantity
    };
    await axios.put("http://127.0.0.1:8000/api/products/"+id+"/", data )
    .then(response => {
      console.log(response);
      let temp_array = this.state.options;
      temp_array[index].quantity_available = response.data.quantity_available;
      this.setState({
        options: temp_array
      })
    });
    
    
  }

  handleChange = (event) => {
    this.setState({
      name: event.target.value
    });  
  }

  handleChange2 = (event) => {
    this.setState({
      rate: event.target.value
    });  
  }

  handleChange3 = (event) => {
    this.setState({
      quantity: event.target.value
    });  
  }
  
  async handleAdd (){
    let data = {
      label: this.state.name,
      value: this.state.rate,
      quantity_available: this.state.quantity
    };
    await axios.post("http://127.0.0.1:8000/api/products/",data)
    .then(response => {
      console.log(response);
      let temp_array = this.state.options;
      temp_array = temp_array.concat(response.data);

      this.setState({
        options: temp_array
      })
    });
  }

  async handleDelete(id, index) {
    await axios.delete("http://127.0.0.1:8000/api/products/"+id+"/",id)
    .then(response => {
      console.log(response);
      let temp_array = this.state.options;
      temp_array.splice(index,1);
      this.setState({
        options: temp_array
      })
    })
  }

  render() {
    return(
      <div>
        <Table bordered responsive>
          <thead>
            <tr>
              <th>Product ID</th>
              <th>Product</th>
              <th>Rate</th>
              <th>Quantity Avaliable</th>
            </tr>
          </thead>
          <tbody>
            { this.state.options.map((product, index) => {
              return (
                <tr>
                  <th scope='row'>{product.id}</th>
                  <td onClick={() => this.handleNameClick(index, product.label, product.id, product.value, product.quantity_available)}>{product.label}</td>
                  <td onClick={() => this.handleRateClick(index, product.label, product.id, product.value, product.quantity_available)}>{product.value}</td>
                  <td onClick={() => this.handleQuantityClick(index, product.label, product.id, product.value, product.quantity_available)}>{product.quantity_available}</td>
                  <td onClick={() => this.handleDelete(product.id, index)}><IoAndroidDelete /></td>
                </tr>
              )
            })}
          </tbody>
        </Table>

        <AddProduct 
        handleAdd={this.handleAdd}
        handleChange={this.handleChange}
        handleChange2={this.handleChange2}
        handleChange3={this.handleChange3}/>
      </div>
    );
  }
}
