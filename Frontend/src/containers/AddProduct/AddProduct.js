import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';

export class AddProduct extends React.Component {

  render() {
    
    return(

      <Form inline>
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
          <Label for="productname" className="mr-sm-2"></Label>
          <Input type="text" name="productname" id="productname" onChange={this.props.handleChange} placeholder="Name your product" />
        </FormGroup>
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
          <Label for="productrate" className="mr-sm-2"></Label>
          <Input type="number" name="productrate" id="productrate" onChange={this.props.handleChange2}  placeholder="Rate Per Unit" />
        </FormGroup>
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
        <Label for="productrate" className="mr-sm-2"></Label>
          <Input type="number" name="productrate" id="productrate" onChange={this.props.handleChange3} placeholder="Quantity Available" />
        </FormGroup>
        <Button onClick={this.props.handleAdd} color='success'>Add</Button>
      </Form>
    );  
  }
}