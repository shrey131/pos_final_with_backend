import React from 'react';
import './pos.css';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import {Button,Container, Row, Col, Table} from 'reactstrap';
import Background from '../../images/pos.jpg';

import {fetchProducts} from '../../api';
import {Checkout} from '../Checkout/Checkout';
import axios from 'axios';
import qrcode from 'react-qr-code';
import AnimatedNumber from 'react-animated-number';
import {ProductList} from '../../components/ProductList/ProductList';


const sectionStyle = {
  width: "100%",
  height: "100%",
  backgroundImage: `url(${Background})`
};
const style= {
  color: 'white'
};
export class POSScreen extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      grandTotal: 0,
      selectedItemsList: [],
      options: [],
      selectedOption: '' ,
      bill_id: 1,
    };
    this.incrementItem = this.incrementItem.bind(this);
    this.decrementItem = this.decrementItem.bind(this);
    this.getData = this.getData.bind(this);
    this.addToCart = this.addToCart.bind(this);
    this.handleDeleteItem = this.handleDeleteItem.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCheckout = this.handleCheckout.bind(this);
  }

  componentDidMount() {
    this.getData();

  }

  async getData() {
    let data = await fetchProducts();
    let data1 = axios.get("http://127.0.0.1:8000/api/bill/")
    .then(response => {

      this.setState({bill_id: response.data.length+1});


    });
    this.setState({options: data});



  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
  }

  incrementItem = (index) => {
    let temp_array = this.state.selectedItemsList;
    let temp_grandtotal = this.state.grandTotal;
    temp_array[index].quantity_selected += 1;
    temp_array[index].total = temp_array[index].value * temp_array[index].quantity_selected;
    temp_grandtotal += temp_array[index].value;
    this.setState({selectedItemsList: temp_array, grandTotal: temp_grandtotal});
  }

  decrementItem = (index) => {
    let temp_grandtotal = this.state.grandTotal;
    let temp_array = this.state.selectedItemsList;
    if (temp_array[index].quantity_selected !== 0){
      temp_array[index].quantity_selected -= 1;
      temp_array[index].total = temp_array[index].value * temp_array[index].quantity_selected;
      temp_grandtotal -= temp_array[index].value;
      this.setState({selectedItemsList: temp_array, grandTotal: temp_grandtotal});
    }
    else {
      console.log("Error. Quantity cannnot be negative");
    }
  }

  addToCart = () => {
    let temp_array = this.state.selectedItemsList;
    let checkMultipleProductsCounter = 0;
    temp_array.map(product => {
      if(this.state.selectedOption.label === product.label){
         checkMultipleProductsCounter += 1;
      }
      return 'x';
    });

    if (this.state.selectedOption.label == null) {
      console.log("Error. No product is selected. Usage: Select a product, then click on 'Add to cart'.");
    }
    else if (checkMultipleProductsCounter > 0) {
      console.log("Error. Product is already present in your shopping cart. Multiple entries of the same product is not allowed. Increase or decrease the quantity from the list.");
    }
    else{
      let tempArray = [
        {
          id: this.state.selectedOption.id,
          label: this.state.selectedOption.label,
          value: this.state.selectedOption.value,
          quantity_available: this.state.selectedOption.quantity_available,
          quantity_selected: 1,
          total: this.state.selectedOption.value
        },
      ];
      temp_array = temp_array.concat(tempArray);
      this.setState(
        {
          selectedItemsList: temp_array,
          selectedOption : '',
          grandTotal: this.state.grandTotal+this.state.selectedOption.value
        }
      );


    }

  }

  handleDeleteItem = (index) => {
    let temp_grandtotal = this.state.grandTotal;
    let temp_array = this.state.selectedItemsList;
    const itemTotal = this.state.selectedItemsList[index].value * this.state.selectedItemsList[index].quantity_selected  ;
    temp_grandtotal = temp_grandtotal - itemTotal ;
    temp_array.splice(index,1);
    this.setState({selectedItemsList: temp_array, grandTotal: temp_grandtotal});
  }

  handleCheckout = () => {
    this.state.selectedItemsList.map(product => {
      let data = {
        bill_id: this.state.bill_id,
        label: product.label,
        value: product.value,
        quantity: product.quantity_selected,
        total: this.state.grandTotal,
      };

      axios.post("http://127.0.0.1:8000/api/bill/",data)

    })

    this.setState({
      grandTotal: 0,
      selectedItemsList: [],
    })

  }

  render () {

    let checkoutButton = (
      <Col sm={{ size: '2',offset: 2}}>
      <a href="/qrcode"><Button onClick={this.handleCheckout} color="success">Checkout</Button></a>
      </Col>
    );

    const { selectedOption } = this.state;



		return (
          <div className = "abcd" style = {sectionStyle} >
          <div className = "abc">
            <Container>
              <Row>
                <Col lg={{ size: 6, order: 2, offset: 1 }}>
                  <Select
                  name="Select the product"
                  value={selectedOption}
                  onChange={this.handleChange}
                  options={this.state.options}
                  />
                </Col>
                <Col lg={{ size: 2, order: 2, offset: 0 }}>
                  <Button color="success" size="md" onClick={this.addToCart}>Add to cart</Button>
                </Col>
              </Row>
              {this.state.selectedItemsList.length > 0 ?
                <Row style={{padding: 25}}>
                  <Col>
                <ProductList
                ItemsList={this.state.selectedItemsList}
                incrementItem={this.incrementItem}
                decrementItem={this.decrementItem}
                deleteItem={this.handleDeleteItem}/>
                </Col>
                </Row>
                :null
              }
              {
                this.state.grandTotal > 0 ?
                (
                  <Row>
                    <Col sm={{ size: '2', offset: 4}}>
                      <p style={{color: 'black', fontSize: 20}}>
                        Grand Total:  <AnimatedNumber
                          component="text"
                          value={this.state.grandTotal}
                          style={{
                            color: 'black',
                            transition: '0.8s ease-out',
                            fontSize: 30,
                            transitionProperty:' color, opacity'
                          }}
                          frameStyle={perc => (
                            perc === 100 ? {} : {backgroundColor: '#cccccc'}
                          )}
                          duration={300}
                          />
                      </p>
                    </Col>
                    <Col sm={{ size: '2',offset: 2}}>
                      <a href="/qrcode"><Button onClick={this.handleCheckout} color="success">Checkout</Button></a>
                    </Col>
                  </Row>
                ):
                null
              }
            </Container>

            </div>
          </div>


		);
	}
}
