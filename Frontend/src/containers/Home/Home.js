import React from 'react';
import './Home.css';
import {Thing} from '../../modals/ThingsToDo/ThingsToDo';
import CSSTransition from 'react-transition-group/CSSTransition';
import TransitionGroup from 'react-transition-group/TransitionGroup';


export class Home extends React.Component {
  render(){
    let array =[];
    array = [
      {
        thing: "Abc",
        definition: "abc"
      },
      {
        thing: "Def",
        definition: "def"
      },
      {
        thing: "Ghi",
        definition: "ghi"
      },
      {
        thing: "Jkl",
        definition: "jkl"
      },
      {
        thing: "Mno",
        definition: "mno"
      },
      {
        thing: "Pqr",
        definition: "pqr"
      }
    ];
    let variable = array.map((item, index) => {
      return (
       <CSSTransition key={index} classNames="fade" timeout={1000}>
         <Thing thing={item.thing} definition={item.definition} />
       </CSSTransition>
      )});
    return (
      <div className="Home">
       <TransitionGroup component={null}>
         {variable}
       </TransitionGroup>
      </div>
    )
  }
}
